import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import Header from './components/Header';

import { Routes, Route, Link } from "react-router-dom";
import EvenOdd from './pages/EvenOdd';
import { Rating } from '@mui/material';

function App() {
  return (
    <div className="App">
      <Header />
      <Rating/>
      <Routes>
        <Route path="about" element={
               <AboutUsPage />
        } /> 
        <Route path="+" element={
              <EvenOdd />
        } /> 
        <Route path="-" element={
              <Rating/>
        } /> 
        </Routes>
    </div>
  );
}

export default App;
