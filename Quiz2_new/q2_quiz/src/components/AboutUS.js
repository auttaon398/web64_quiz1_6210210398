import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function AboutUS (props){

    return ( 
        <Box sx={{ width :" 30%"}}>
            <Paper elevation={3} >
                <h2>report by : { props.name }</h2>
                <h3>telephone number : { props.address } </h3>
                <h3>email : { props.province } </h3>
            </Paper>  
        </Box>
    );
}

export default AboutUS;