import React, { useState } from 'react'
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { Grid  } from '@mui/material';

function EvenOdd ( ) {

    const [username,setUsername] = useState()
    const [number,setNumber] = useState("")
    const [transnum , setTransnum ] = useState("");
    
    React.useEffect(()=>{
      setTimeout(()=>{
      setUsername("Hello Guy")
        })})
      
      function change (){  
      if ( number == 0 ){
        setTransnum("The number is zero")
      }else if( number%2 == 1 ){
        setTransnum("The number is odd")
      }else {
        setTransnum("The number is even")
        }
      }
 return (
      <Grid container spacing={2} sx={{ marginTop : "5px"}}>
        <Grid item xs={12}>
        <Typography variant="h4" gutterBottom component="div">
        {!username ? <div>Loading...</div> : <h1> {username}!</h1>}
          number : <input type="text"
          value={ number }
          onChange= {  (e) => {setNumber(e.target.value); }} /> <br />
         </Typography>
        </Grid>
        <Grid item xs={12}>
        <Typography variant="h4" gutterBottom component="div">
        <h3> check : { transnum }</h3>
         <Button  variant="contained" onClick={ change  }> Check </Button>
         </Typography>
        </Grid>
  
      </Grid>
   );
}
export default EvenOdd;
